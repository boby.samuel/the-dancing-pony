import {
  CallHandler,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, catchError, map } from 'rxjs';
import { ValidationError } from 'sequelize';

@Injectable()
export class ResponseInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map(async (data) => {
        const res = context.switchToHttp().getResponse();
        const formatRes = {
          status: true,
          data,
        };

        return formatRes;
      }),
      catchError((err) => {
        const res = {
          code: HttpStatus.INTERNAL_SERVER_ERROR,
          status: false,
          error: err.message,
        };

        if (err instanceof ValidationError) {
          const errData: Record<string, any> = {};

          err.errors.map((err) => {
            errData[err.path] = err.type;
          });

          const errCode = HttpStatus.UNPROCESSABLE_ENTITY;
          res.code = errCode;
          res.error = errData;
        } else if (err instanceof HttpException) {
          let fullRes = err.getResponse();

          if (typeof fullRes === 'object' && fullRes['code']) {
            fullRes = fullRes['error'];
          }

          res.code = err.getStatus();
          res.error = fullRes;
        } else if (err?.parent?.code == '23505') {
          const errCode = HttpStatus.UNPROCESSABLE_ENTITY;
          res.code = errCode;
          res.error = err.errors;
        }

        throw new HttpException(res, res.code);
      }),
    );
  }
}
