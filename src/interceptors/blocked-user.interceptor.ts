import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
  UnauthorizedException,
} from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class BlockedUsersInterceptor implements NestInterceptor {
  private blockedUsers = ['Sméagol'];
  constructor() {}
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request = context.switchToHttp().getRequest();
    const username = request?.user?.username || 'guest';

    if (this.blockedUsers.includes(username)) {
      throw new UnauthorizedException('You are not allowes to access');
    }

    return next.handle();
  }
}
