import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { Type } from 'class-transformer';

export class CreateDishDto {
  @ApiProperty({ required: true, type: String })
  @IsNotEmpty()
  @IsString()
  name?: string;

  @ApiProperty({ required: true, type: String })
  @IsNotEmpty()
  @IsString()
  description?: string;

  @ApiProperty({ required: true, type: String })
  @IsNotEmpty()
  @IsString()
  image?: string;

  @ApiProperty({ required: true, type: Number })
  @IsNotEmpty()
  @IsNumber()
  @Type(() => Number)
  price?: number;
}
