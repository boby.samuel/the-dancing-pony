import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { Type } from 'class-transformer';

export class UpdateDishDto {
  @ApiProperty({ required: false, type: String })
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty({ required: false, type: String })
  @IsOptional()
  @IsString()
  description?: string;

  @ApiProperty({ required: false, type: String })
  @IsOptional()
  @IsString()
  image?: string;

  @ApiProperty({ required: false, type: Number })
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  price?: number;
}
