import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { PaginationDto } from 'src/commons/pagination.dto';

export class GetManyDishDto extends PaginationDto {
  @ApiProperty({ required: false, type: String })
  @IsOptional()
  keyword?: string;
}
