import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { DishesService } from './dishes.service';
import {
  CreateDishDto,
  DishRatingDto,
  GetManyDishDto,
  UpdateDishDto,
} from './dto';
import { BlockedUsersInterceptor } from 'src/interceptors';

@Controller('dishes')
@ApiTags('Dishes')
@ApiBearerAuth()
export class DishesController {
  constructor(private dishService: DishesService) {}

  @Get()
  async getAllDish(@Query() getManyDishDto: GetManyDishDto) {
    return this.dishService.getMany(getManyDishDto);
  }

  @Get(':id')
  async getOneDish(@Param('id') id: number) {
    return this.dishService.getOne(id);
  }

  @Post()
  async createDish(@Body() payload: CreateDishDto) {
    return await this.dishService.create(payload);
  }

  @Patch(':id')
  async updateDish(@Body() payload: UpdateDishDto, @Param('id') id: number) {
    return this.dishService.update(id, payload);
  }

  @Delete(':id')
  async deleteDish(@Param('id') id: number) {
    return await this.dishService.delete(id);
  }

  @Post('rating')
  @UseInterceptors(BlockedUsersInterceptor)
  async rating(@Body() payload: DishRatingDto, @Req() req) {
    return this.dishService.setRating(payload, req);
  }

  @Get(':id/rating')
  async getRating(@Param('id') id: number) {
    return this.dishService.getRating(id);
  }
}
