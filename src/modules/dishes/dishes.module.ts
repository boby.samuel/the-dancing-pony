import { Module } from '@nestjs/common';
import { DishesController } from './dishes.controller';
import { DishesService } from './dishes.service';
import { SequelizeModule } from '@nestjs/sequelize';
import * as DishEntities from './entity';

@Module({
  imports: [SequelizeModule.forFeature(Object.values(DishEntities))],
  controllers: [DishesController],
  providers: [DishesService],
})
export class DishesModule {}
