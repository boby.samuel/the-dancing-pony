import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Dish, DishRating } from './entity';
import {
  CreateDishDto,
  DishRatingDto,
  GetManyDishDto,
  UpdateDishDto,
} from './dto';
import { Op } from 'sequelize';

@Injectable()
export class DishesService {
  constructor(
    @InjectModel(Dish)
    private readonly dishModel: typeof Dish,
    @InjectModel(DishRating)
    private readonly ratingModel: typeof DishRating,
  ) {}

  async getMany(getManyDishDto: GetManyDishDto) {
    const { page, limit, keyword } = getManyDishDto;
    const offset = (page - 1) * limit;
    let query = {};

    if (keyword) {
      query['name'] = { [Op.iLike]: `%${keyword}%` };
    }
    return await this.dishModel.findAll({ where: query, limit, offset });
  }

  async getOne(id: number) {
    try {
      const queryOptions = {
        attributes: { exclude: ['createdAt', 'updatedAt'] },
      };
      const dish = await this.dishModel.findByPk(id, queryOptions);
      return dish;
    } catch (err) {
      throw err;
    }
  }

  async create(payload: CreateDishDto) {
    return await this.dishModel.create(payload);
  }

  async delete(id: number) {
    await this.dishModel.destroy({ where: { id } });
    return 'dish deleted';
  }

  async update(id: number, payload: UpdateDishDto) {
    try {
      const dish = await this.dishModel.findByPk(id);
      if (!dish) {
        throw new NotFoundException(`dish not found`);
      }

      await dish.update(payload);
    } catch (err) {
      throw err;
    }
  }

  async setRating(payload: DishRatingDto, req) {
    try {
      const { id: uid } = req.user;
      const rated = await this.ratingModel.findOne({
        where: {
          user_id: uid,
          dish_id: payload.id,
        },
        attributes: ['id'],
      });

      if (rated) {
        throw new HttpException(
          'You have rated this dish',
          HttpStatus.BAD_REQUEST,
        );
      }

      await this.ratingModel.create({
        user_id: uid,
        dish_id: payload.id,
        rating: payload.rating,
      });
    } catch (err) {
      throw err;
    }
  }

  async getRating(id: number) {
    const ratings = await this.ratingModel.findAll({
      where: { id },
      attributes: ['rating'],
    });

    const sum = ratings.reduce((sum, dish) => {
      sum += dish.rating;
      return sum;
    }, 0);

    const avg = Math.floor(sum / ratings.length);
    return { avg_rating: avg };
  }
}
