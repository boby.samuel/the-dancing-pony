import { Column, DataType, Model, Table } from 'sequelize-typescript';

@Table({
  timestamps: true,
})
export class DishRating extends Model<DishRating> {
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  dish_id: number;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  user_id: number;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  rating: number;
}
