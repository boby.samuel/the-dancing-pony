import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class RegisterPayloadDTO {
  @ApiProperty({ required: true, type: String })
  @IsNotEmpty()
  @IsString()
  username?: string;

  @ApiProperty({ required: true, type: String })
  @IsNotEmpty()
  @IsString()
  nickname?: string;

  @ApiProperty({ required: true, type: String })
  @IsNotEmpty()
  @IsString()
  password?: string;
}
