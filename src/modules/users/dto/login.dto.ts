import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class LoginPayloadDTO {
  @ApiProperty({ required: true, type: String })
  @IsNotEmpty()
  @IsString()
  username?: string;

  @ApiProperty({ required: true, type: String })
  @IsNotEmpty()
  @IsString()
  password?: string;
}
