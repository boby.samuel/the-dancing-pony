import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User, UserBlocked } from './entity';
import { LoginPayloadDTO, RegisterPayloadDTO } from './dto';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User)
    private readonly userModel: typeof User,
    @InjectModel(UserBlocked)
    private readonly blockedUserModel: typeof UserBlocked,
    private readonly jwtService: JwtService,
  ) {}

  async getMany() {
    return await this.userModel.findAll({});
  }

  async login(payload: LoginPayloadDTO) {
    try {
      const { username, password } = payload;
      const userExists = await this.userModel.findOne({
        where: { username },
      });

      if (!userExists) {
        throw new UnauthorizedException(`username ${username} not found`);
      }

      const validPassword = await bcrypt.compare(password, userExists.password);
      if (!validPassword) {
        throw new UnauthorizedException(`Wrong Password!`);
      }

      // create JWT
      const jwtPayload = {
        id: userExists.id,
        username: userExists.username,
        nickname: userExists.nickname,
      };
      const accessToken = await this.jwtService.signAsync(jwtPayload);

      // save JWT Token to db for validation
      return { accessToken };
    } catch (err) {
      throw err;
    }
  }

  async register(payload: RegisterPayloadDTO) {
    try {
      const { username, nickname, password } = payload;
      const usernameExists = await this.userModel.findOne({
        where: { username },
      });

      if (usernameExists) {
        throw new HttpException(
          `username ${username} already exist`,
          HttpStatus.BAD_REQUEST,
        );
      }

      const hashedPassword = await bcrypt.hash(
        password,
        +process.env.SALT_ROUNDS,
      );

      const userPayload = { username, nickname, password: hashedPassword };
      await this.userModel.create(userPayload);

      return 'registration completed';
    } catch (err) {
      throw err;
    }
  }

  async blockedUser(username, resource) {
    return await this.blockedUserModel.findOne({
      where: {
        username,
        resource_blocked: resource,
      },
    });
  }
}
