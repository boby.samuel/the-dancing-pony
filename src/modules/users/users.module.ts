import { Global, Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { User, UserToken, UserBlocked } from './entity';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/commons/jwt-secret.constant';

@Module({
  imports: [
    SequelizeModule.forFeature(Object.values([User, UserToken, UserBlocked])),
    JwtModule.register({
      global: true,
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '1d' },
    }),
  ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
