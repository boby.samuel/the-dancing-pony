import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UsersService } from './users.service';
import { LoginPayloadDTO, RegisterPayloadDTO } from './dto';
import { Public } from 'src/decorators';

@Controller('users')
@ApiTags('Users')
@ApiBearerAuth()
export class UsersController {
  constructor(private userService: UsersService) {}

  @Public()
  @Get()
  async users() {
    return this.userService.getMany();
  }

  @Post('login')
  @Public()
  async login(@Body() payload: LoginPayloadDTO) {
    return this.userService.login(payload);
  }

  @Public()
  @Post('register')
  async register(@Body() payload: RegisterPayloadDTO) {
    return this.userService.register(payload);
  }
}
