import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateIf,
} from 'class-validator';
// import { ValidationMessage } from "../constant";

export class PaginationDto {
  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  @ApiProperty({
    required: true,
    type: Number,
  })
  @IsInt()
  @Type(() => Number)
  @IsNotEmpty()
  page: number;

  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  @ApiProperty({ required: true, type: Number })
  @IsInt()
  @Type(() => Number)
  @IsNotEmpty()
  limit: number;

  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  @ApiProperty({
    required: false,
    type: String,
  })
  @IsOptional()
  @IsString()
  sort_field: string;

  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  @ApiProperty({
    required: false,
    enum: ['ASC', 'DESC'],
  })
  sort_dir: string;
}
