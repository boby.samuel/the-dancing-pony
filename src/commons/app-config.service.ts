/*
https://docs.nestjs.com/providers#services
*/

import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
// import { MongoMemoryServer } from "mongodb-memory-server";

@Injectable()
export class AppConfigService {
  constructor(private readonly configService: ConfigService) {}
}
