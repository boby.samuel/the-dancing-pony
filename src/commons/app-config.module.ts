/*
https://docs.nestjs.com/modules
*/

import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { AppConfigService } from './app-config.service';
import * as dotenv from 'dotenv';
dotenv.config();

@Module({
  exports: [AppConfigService],
  imports: [
    ConfigModule.forRoot({
      envFilePath: `${
        process.env.NODE_ENV ? process.cwd() + '/' + process.env.NODE_ENV : ''
      }.env`,
      validationSchema: Joi.object({
        APPS_PORT: Joi.number().required().default(3000),
        DATABASE_DIALECT: Joi.string().required(),
        DATABASE_HOST: Joi.string().required(),
        DATABASE_PORT: Joi.string().required(),
        DATABASE_USER: Joi.string().required(),
        DATABASE_PASS: Joi.string().required(),
        DATABASE_NAME: Joi.string().required(),
      }),
      isGlobal: true,
    }),
  ],
  providers: [AppConfigService],
})
export class AppConfigModule {}
