import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  HttpException,
  HttpStatus,
  Logger,
  ValidationPipe,
  VersioningType,
} from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  app.setGlobalPrefix('api');
  app.enableVersioning({ type: VersioningType.URI, defaultVersion: '1' });

  app.useGlobalPipes(
    new ValidationPipe({
      always: true,
      stopAtFirstError: true,
      whitelist: true,
      transform: true,
      exceptionFactory: (errors) => {
        const mapped = {};
        errors.map((error) => {
          const errorMapper = (
            errorData: Record<string, any>,
            errorMap: Record<string, any>,
          ) => {
            console.dir({ errorData }, { depth: null });
            if (!errorData.children.length) {
              const msg = Object.values(errorData.constraints);

              errorMap[errorData.property] = msg[0];
            } else {
              errorData.children.map((item) => {
                if (item.children.length) errorMap[item.property] = {};

                return errorMapper(
                  item,
                  item.children.length ? errorMap[item.property] : errorMap,
                );
              });
            }
          };

          if (error.children.length) {
            mapped[error.property] = {};
            errorMapper(error, mapped[error.property]);
          } else {
            const msg = Object.values(error.constraints);
            const keys = Object.keys(error.constraints);
            mapped[error.property] = keys[0] === 'isNotEmpty' ? 'hehe' : msg[0];
          }
        });

        throw new HttpException(mapped, HttpStatus.UNPROCESSABLE_ENTITY);
      },
    }),
  );

  const configSwagger = new DocumentBuilder()
    .setTitle('THE DANCING PONY')
    .setVersion('0.1')
    .addBearerAuth({
      description: `Harus dengan format: Bearer <JWT>`,
      name: 'Authorization',
      bearerFormat: 'Bearer',
      scheme: 'Bearer',
      type: 'http',
      in: 'Header',
    })
    .build();

  const document = SwaggerModule.createDocument(app, configSwagger);
  SwaggerModule.setup('docs', app, document, {
    swaggerOptions: {
      docExpansion: 'none',
      persistAuthorization: true,
      displayRequestDuration: true,
    },
  });

  const APP_PORT = process.env.APPS_PORT;
  await app.listen(APP_PORT);
  new Logger().verbose('Apps running on port ' + APP_PORT);
}
bootstrap();
